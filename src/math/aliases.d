module math.aliases;

import math.vector;
import math.matrix;
import math.quaternion;

alias vec2 = Vector!(float, 2);
alias vec3 = Vector!(float, 3);
alias vec4 = Vector!(float, 4);

alias dvec2 = Vector!(double, 2);
alias dvec3 = Vector!(double, 3);
alias dvec4 = Vector!(double, 4);

alias ubvec2 = Vector!(ubyte, 2);
alias ubvec3 = Vector!(ubyte, 3);
alias ubvec4 = Vector!(ubyte, 4);

alias bvec2 = Vector!(byte, 2);
alias bvec3 = Vector!(byte, 3);
alias bvec4 = Vector!(byte, 4);

alias usvec2 = Vector!(ushort, 2);
alias usvec3 = Vector!(ushort, 3);
alias usvec4 = Vector!(ushort, 4);

alias svec2 = Vector!(short, 2);
alias svec3 = Vector!(short, 3);
alias svec4 = Vector!(short, 4);

alias ivec2 = Vector!(int, 2);
alias ivec3 = Vector!(int, 3);
alias ivec4 = Vector!(int, 4);

alias uvec2 = Vector!(uint, 2);
alias uvec3 = Vector!(uint, 3);
alias uvec4 = Vector!(uint, 4);

alias mat4 = Matrix!(float, 4, 4);

alias quat = Quaternion!float;
