module math.traits;

import std.traits;
import std.meta;

bool allImplicitlyConvertible(To, From...)()
{
    import math.vector : Vector;

    bool ret = true;
    foreach (f; From)
    {
        static if (isScalarType!f && !isImplicitlyConvertible!(f, To))
            ret = false;
        else static if (is(f == Vector!(T, d), T, size_t d)
                && !isImplicitlyConvertible!(f.type, To))
            ret = false;
    }
    return ret;
}

bool allScalarTypes(T...)()
{
    bool ret = true;
    foreach (t; T)
    {
        static if (!isScalarType!t)
            ret = false;
    }
    return ret;
}

bool matchingDimensions(size_t dims, T...)()
{
    import math;

    size_t value;
    foreach (i, t; T)
    {
        static if (isScalarType!t)
            value += 1;
        else static if (is(t == Vector!(U, d), U, size_t d))
            value += d;
        else
            static assert(false, "Invalid type.");
    }

    return value == dims;
}

auto mixedArgumentIndex(size_t i, T...)()
{
    import math;

    size_t ret;
    foreach (idx, t; T[0 .. i])
    {
        static if (isScalarType!t)
            ret += 1;
        else static if (is(t == Vector!(U, d), U, size_t d))
            ret += d;
        else
            static assert(false, "Invalid type.");
    }
    return ret;
}

template CTRange(size_t from, size_t to)
{
    static if (from == to)
        alias CTRange = AliasSeq!();
    else
        alias CTRange = AliasSeq!(from, CTRange!(from + 1, to));
}

template isValidAccessor(char acc, size_t Dims)
{
    static if ((acc == 'x' || acc == 'r' || acc == 's') && Dims >= 1)
        enum isValidAccessor = true;
    else static if ((acc == 'y' || acc == 'g' || acc == 't') && Dims >= 2)
        enum isValidAccessor = true;
    else static if ((acc == 'z' || acc == 'b' || acc == 'p') && Dims >= 3)
        enum isValidAccessor = true;
    else static if ((acc == 'w' || acc == 'a' || acc == 'q') && Dims >= 4)
        enum isValidAccessor = true;
    else
        static assert(false, acc ~ " is not a valid accessor.");
}

template allValidAccessors(string accessors, size_t Dims)
{
    static if (accessors.length == 0)
        enum allValidAccessors = true;
    else
        enum allValidAccessors = isValidAccessor!(accessors[0], Dims)
            && allValidAccessors!(accessors[1 .. $], Dims);
}

bool hasNoDuplicates(string s)()
{
    foreach (i; CTRange!(0, s.length))
    {
        foreach (i2; CTRange!(i + 1, s.length))
        {
            if (s[i] == s[i2])
                return false;
        }
        // static assert(s[i] != s[i2], "Contains duplicate: "~s[i]);
    }
    return true;
}

template isVector(T)
{
    import math.vector;

    static if (is(T == Vector!(U, Dims), U, size_t Dims))
        enum isVector = true;
    else
        enum isVector = false;
}

enum Unit
{
    radian,
    degree
}
