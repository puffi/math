module math.quaternion;

import std.traits : isFloatingPoint, isScalarType;
import math.vector;

struct Quaternion(T) if (isFloatingPoint!T)
{
    alias type = T;

    private
    {
        T[4] mData = [1, 0, 0, 0];
    }

    this(T w, T x, T y, T z)
    {
        mData[0] = w;
        mData[1] = x;
        mData[2] = y;
        mData[3] = z;
    }

    @property auto w() const
    {
        return mData[0];
    }

    @property void w(T v)
    {
        mData[0] = v;
    }

    @property auto x() const
    {
        return mData[1];
    }

    @property void x(T v)
    {
        mData[1] = v;
    }

    @property auto y() const
    {
        return mData[2];
    }

    @property void y(T v)
    {
        mData[2] = v;
    }

    @property auto z() const
    {
        return mData[3];
    }

    @property void z(T v)
    {
        mData[3] = v;
    }

    auto opUnary(string s : "-")() const
    {
        return Quaternion!T(w, -x, -y, -z);
    }

    auto opBinary(string s)(Quaternion!T rhs) const if (s == "*")
    {
        return Quaternion!T(w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z,
                w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y,
                w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x,
                w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w);
    }

    auto opBinary(string op)(Quaternion!T rhs) const if (op == "+" || op == "-")
    {
        return Quaternion!T(mixin("w " ~ op ~ " rhs.w"),
                mixin("x " ~ op ~ " rhs.x"), mixin("y " ~ op ~ " rhs.y"), mixin("z " ~ op ~ " rhs.z"));
    }

    auto opBinary(string s)(Vector!(T, 3) rhs) const if (s == "*")
    {
        Quaternion!T vecQuat = Quaternion!T(0, rhs.x, rhs.y, rhs.z);
        vecQuat = this * vecQuat * (-this);
        return Vector!(T, 3)(vecQuat.x, vecQuat.y, vecQuat.z);
    }

    auto opBinaryRight(string s)(Vector!(T, 3) rhs) const if (s == "*")
    {
        return opBinary!(s)(rhs);
    }

    auto opBinary(string op, Arg)(Arg rhs) const 
            if ((op == "+" || op == "-" || op == "*" || op == "/") && isScalarType!Arg)
    {
        mixin(
                "return Quaternion!T(w " ~ op ~ " rhs, x " ~ op ~ " rhs, y " ~ op
                ~ " rhs, z " ~ op ~ " rhs);");
    }

    auto opBinaryRight(string op, Arg)(Arg rhs) const 
            if ((op == "+" || op == "-" || op == "*" || op == "/") && isScalarType!Arg)
    {
        return opBinary!op(rhs);
    }

    static @property auto identity()
    {
        return Quaternion!T();
    }
}
