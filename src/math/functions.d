module math.functions;

import math.matrix;
import math.vector;
import math.quaternion;
import math.traits;
import std.traits;

// matrix

auto translate(Vec)(in auto ref Vec v)
        if (is(Vec == Vector!(T, Dims), T, size_t Dims) && Dims > 0 && Dims < 4)
{
    Matrix!(Vec.type, 4, 4) ret = Matrix!(Vec.type, 4, 4).identity;
    foreach (i; CTRange!(0, Vec.dimensions))
    {
        ret[3][i] = v[i];
    }
    return ret;
}

alias Unit = math.traits.Unit;

auto rotate(Unit unit = Unit.radian, T)(in T rot) if (isFloatingPoint!T)
{
    Matrix!(T, 4, 4) mat = Matrix!(T, 4, 4).identity;

    import std.math : cos, sin;

    static if (unit == Unit.radian)
    {
        auto c = rot.cos;
        auto s = rot.sin;
    }
    else
    {
        auto rad = rot.toRadian;
        auto c = rad.cos;
        auto s = rad.sin;
    }

    mat[0][0] = c;
    mat[0][1] = s;
    mat[1][0] = -s;
    mat[1][1] = c;

    return mat;
}

auto rotate(T)(in auto ref T quat) if (is(T == Quaternion!U, U))
{
    Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;

    mat[0][0] = 1 - 2 * quat.y * quat.y - 2 * quat.z * quat.z;
    mat[0][1] = 2 * quat.x * quat.y + 2 * quat.w * quat.z;
    mat[0][2] = 2 * quat.x * quat.z - 2 * quat.w * quat.y;
    mat[1][0] = 2 * quat.x * quat.y - 2 * quat.w * quat.z;
    mat[1][1] = 1 - 2 * quat.x * quat.x - 2 * quat.z * quat.z;
    mat[1][2] = 2 * quat.y * quat.z + 2 * quat.w * quat.x;
    mat[2][0] = 2 * quat.x * quat.z + 2 * quat.w * quat.y;
    mat[2][1] = 2 * quat.y * quat.z - 2 * quat.w * quat.x;
    mat[2][2] = 1 - 2 * quat.x * quat.x - 2 * quat.y * quat.y;

    return mat;
}

auto scale(Vec)(in auto ref Vec v)
        if (is(Vec == Vector!(T, Dims), T, size_t Dims) && Dims > 0 && Dims < 4)
{
    Matrix!(Vec.type, 4, 4) ret = Matrix!(Vec.type, 4, 4).identity;
    foreach (i; CTRange!(0, Vec.dimensions))
    {
        ret[i][i] = v[i];
    }
    return ret;
}

// based on https://www.geometrictools.com/Documentation/LaplaceExpansionTheorem.pdf
auto invert(Mat)(in auto ref Mat mat) if (is(Mat == Matrix!(T, 4, 4), T))
{
    import std.stdio;

    auto s0 = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
    auto s1 = mat[0][0] * mat[2][1] - mat[0][1] * mat[2][0];
    auto s2 = mat[0][0] * mat[3][1] - mat[0][1] * mat[3][0];
    auto s3 = mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0];
    auto s4 = mat[1][0] * mat[3][1] - mat[1][1] * mat[3][0];
    auto s5 = mat[2][0] * mat[3][1] - mat[2][1] * mat[3][0];
    auto c5 = mat[2][2] * mat[3][3] - mat[2][3] * mat[3][2];
    auto c4 = mat[1][2] * mat[3][3] - mat[1][3] * mat[3][2];
    auto c3 = mat[1][2] * mat[2][3] - mat[1][3] * mat[2][2];
    auto c2 = mat[0][2] * mat[3][3] - mat[0][3] * mat[3][2];
    auto c1 = mat[0][2] * mat[2][3] - mat[0][3] * mat[2][2];
    auto c0 = mat[0][2] * mat[1][3] - mat[0][3] * mat[1][2];

    auto det = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
    assert(det != 0, "determinant is zero");
    auto invDet = 1 / det;

    Mat ret;
    ret[0][0] = mat[1][1] * c5 - mat[2][1] * c4 + mat[3][1] * c3;
    ret[0][1] = -mat[0][1] * c5 + mat[2][1] * c2 - mat[3][1] * c1;
    ret[0][2] = mat[0][1] * c4 - mat[1][1] * c2 + mat[3][1] * c0;
    ret[0][3] = -mat[0][1] * c3 + mat[1][1] * c1 - mat[2][1] * c0;

    ret[1][0] = -mat[1][0] * c5 + mat[2][0] * c4 - mat[3][0] * c3;
    ret[1][1] = mat[0][0] * c5 - mat[2][0] * c2 + mat[3][0] * c1;
    ret[1][2] = -mat[0][0] * c4 + mat[1][0] * c2 - mat[3][0] * c0;
    ret[1][3] = mat[0][0] * c3 - mat[1][0] * c1 + mat[2][0] * c0;

    ret[2][0] = mat[1][3] * s5 - mat[2][3] * s4 + mat[3][3] * s3;
    ret[2][1] = -mat[0][3] * s5 + mat[2][3] * s2 - mat[3][3] * s1;
    ret[2][2] = mat[0][3] * s4 - mat[1][3] * s2 + mat[3][3] * s0;
    ret[2][3] = -mat[0][3] * s3 + mat[1][3] * s1 - mat[2][3] * s0;

    ret[3][0] = -mat[1][2] * s5 + mat[2][2] * s4 - mat[3][2] * s3;
    ret[3][1] = mat[0][2] * s5 - mat[2][2] * s2 + mat[3][2] * s1;
    ret[3][2] = -mat[0][2] * s4 + mat[1][2] * s2 - mat[3][2] * s0;
    ret[3][3] = mat[0][2] * s3 - mat[1][2] * s1 + mat[2][2] * s0;

    return ret * invDet;
}

auto orthographic(T)(T halfWidth, T halfHeight, T near, T far)
        if (isFloatingPoint!T)
{
    return Matrix!(T, 4, 4)(1 / halfWidth, 0, 0, 0, 0, 1 / halfHeight, 0, 0, 0,
            0, -2 / (far - near), 0, 0, 0, -(far + near) / (far - near), 1);
}

// yFov -> Radian
auto perspective(Unit unit = Unit.radian, T)(T fovY, T aspectRatio, T near, T far)
        if (isFloatingPoint!T)
{
    assert(aspectRatio != 0);
    assert(near != far);

    import std.math : tan;

    static if (unit == Unit.radian)
    {
        T val = tan(fovY / 2);
    }
    else
    {
        auto rad = fovY.toRadian;
        T val = tan(rad / 2);
    }

    return Matrix!(T, 4, 4)(1 / (aspectRatio * val), 0, 0, 0, 0, 1 / val, 0, 0,
            0, 0, -(far + near) / (far - near), -1, 0, 0, -(2 * far * near) / (far - near), 0);
}

// angle

auto toRadian(T)(T angle) if (isFloatingPoint!T)
{
    import std.math : PI;

    return angle * (PI / 180);
}

auto toDegree(T)(T angle) if (isFloatingPoint!T)
{
    import std.math : M_1_PI;

    return angle * (180 * M_1_PI);
}

auto normalize(Unit unit = Unit.radian, T)(T angle) if (isFloatingPoint!T)
{
    import std.math : PI;

    enum PI2 = PI * 2;
    static if (unit == Unit.radian)
    {
        auto v = angle % PI2;
        return v >= 0 ? v : v + PI2;
    }
    else
    {
        auto v = angle % 360;
        return v >= 0 ? v : v + 360;
    }
}

// global
auto clamp(T)(T value, T min, T max) if (isScalarType!T)
{
    if (value < min)
        return min;
    else if (value > max)
        return max;
    return value;
}

// vector

auto dot(T)(in T v1, in T v2) if (isVector!T)
{
    alias ValueType = T.type;
    ValueType ret = 0;
    foreach (i; CTRange!(0, T.dimensions))
    {
        ret += v1[i] * v2[i];
    }
    return ret;
}

auto magnitude2(T)(in T v) if (isVector!T)
{
    return v.dot(v);
}

auto magnitude(T)(in T v) if (isVector!T)
{
    import std.math : sqrt;

    return v.magnitude2.sqrt;
}

auto negate(T)(in T v) if (isVector!T)
{
    return -v;
}

auto normalize(T)(in T v) if (isVector!T)
{
    return v / v.magnitude;
}

auto clamp(T)(in T v, in T min, in T max) if (isVector!T)
{
    T ret;
    foreach (i; CTRange!(0, T.dimensions))
    {
        ret[i] = clamp(v[i], min[i], max[i]);
    }
    return ret;
}

auto rotate(Unit unit = Unit.radian, T)(Vector!(T, 2) v, T rot)
        if (isFloatingPoint!T)
{
    import std.math : sin, cos;

    static if (unit == Unit.radian)
    {
        auto c = rot.cos;
        auto s = rot.sin;
    }
    else
    {
        auto c = rot.toRadian.cos;
        auto s = rot.toRadian.sin;
    }
    return Vector!(T, 2)(v.x * c - v.y * s, v.x * s + v.y * c);
}

// quaternion

auto axisAngle(Unit unit = Unit.radian, T)(in auto ref Vector!(T, 3) axis, in T angle)
{
    import std.math : cos, sin;

    static if (unit == Unit.degree)
        auto rot = angle.toRadian;
    else
        auto rot = angle;

    auto axisNorm = axis.normalize;

    return Quaternion!T(cos(rot / 2), axisNorm.x * sin(rot / 2),
            axisNorm.y * sin(rot / 2), axisNorm.z * sin(rot / 2));
}

auto fromEuler(T)(in auto ref Vector!(T, 3) euler) if (isFloatingPoint!T)
{
    Quaternion!T quat;
    import std.math : cos, sin;

    Vector!(T, 3) c = Vector!(T, 3)(euler.x.cos, euler.y.cos, euler.z.cos);
    Vector!(T, 3) s = Vector!(T, 3)(euler.x.sin, euler.y.sin, euler.z.sin);

    quat.w = c.x * c.y * c.z + s.x * s.y * s.z;
    quat.x = s.x * c.y * c.z - c.x * s.y * s.z;
    quat.y = c.x * s.y * c.z + s.x * c.y * s.z;
    quat.z = c.x * c.y * s.z - s.x * s.y * c.z;

    return quat;
}

auto normalize(T)(in auto ref Quaternion!T quat)
{
    T divLength = 1 / quat.magnitude;

    return Quaternion!T(quat.w * divLength, quat.x * divLength,
            quat.y * divLength, quat.z * divLength);
}

auto conjugate(T)(in auto ref Quaternion!T quat)
{
    return -quat;
}

auto invert(T)(in auto ref Quaternion!T quat)
{
    return quat.conjugate / quat.magnitude2;
}

auto magnitude2(T)(in auto ref Quaternion!T quat)
{
    return dot(quat, quat);
}

auto magnitude(T)(in auto ref Quaternion!T quat)
{
    import std.math : sqrt;

    return quat.magnitude2.sqrt;
}

auto dot(T)(in auto ref Quaternion!T first, in auto ref Quaternion!T second)
{
    return first.w * second.w + first.x * second.x + first.y * second.y + first.z * second.z;
}

auto yaw(T)(in auto ref T quat) if (is(T == Quaternion!U, U))
{
    import std.math : atan2;

    return atan2(2 * (quat.w * quat.y - quat.x * quat.z), 1 - 2 * quat.y * quat.y
            - 2 * quat.z * quat.z);
}

auto pitch(T)(in auto ref T quat) if (is(T == Quaternion!U, U))
{
    import std.math : atan2;

    return atan2(2 * quat.x * quat.w - 2 * quat.y * quat.z, 1 - 2 * quat.x
            * quat.x - 2 * quat.z * quat.z);
}

auto roll(T)(in auto ref T quat) if (is(T == Quaternion!U, U))
{
    import std.math : atan2;

    return atan2(2 * quat.y * quat.w - 2 * quat.x * quat.z, 1 - 2 * quat.y
            * quat.y - 2 * quat.z * quat.z);
}
