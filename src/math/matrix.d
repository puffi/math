module math.matrix;

import std.meta;
import std.traits;

import math.vector;
import math.traits;

struct Matrix(T, size_t Cols, size_t Rows) if (isScalarType!T)
{
    alias type = T;
    alias columns = Cols;
    alias rows = Rows;

    private Vector!(T, Rows)[Cols] mData = Vector!(T, Rows)(0);

    this(U)(U arg) if (isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        foreach (c; CTRange!(0, Cols))
        {
            foreach (r; CTRange!(0, Rows))
            {
                mData[c][r] = cast(T) arg;
            }
        }
    }

    this(U...)(U args) if (allImplicitlyConvertible!(T, U) && U.length == Cols * Rows)
    {
        foreach (c; CTRange!(0, Cols))
        {
            foreach (r; CTRange!(0, Rows))
            {
                mData[c][r] = args[r + c * Rows];
            }
        }
    }

    // accessors

    static if (Cols == Rows)
    {
        static @property auto identity()
        {
            typeof(this) m;
            foreach (c; CTRange!(0, Cols))
            {
                foreach (r; CTRange!(0, Rows))
                {
                    static if (c == r)
                        m.mData[c][r] = T(1);
                }
            }
            return m;
        }
    }

    @property auto ptr() const
    {
        return mData[0].ptr;
    }

    auto opIndex(size_t idx) const
    {
        assert(idx < Cols, "Index out of range.");
        return mData[idx];
    }

    auto ref opIndex(size_t idx)
    {
        assert(idx < Cols, "Index out of range.");
        return mData[idx];
    }

    void opIndexAssign(U)(U arg, size_t idx) if (allImplicitlyConvertible!(T, U))
    {
        assert(idx < Cols, "Index out of range.");
        mData[idx] = arg;
    }

    // operators

    auto opBinary(string s)(in auto ref Matrix!(T, Cols, Rows) rhs) const 
            if (s == "+" || s == "-")
    {
        Matrix!(T, Cols, Rows) mat;
        foreach (c; CTRange!(0, Cols))
            foreach (r; CTRange!(0, Rows))
                mixin("mat.mData[c][r] = mData[c][r]" ~ s ~ " rhs.mData[c][r];");
        return mat;
    }

    auto opBinary(string s, U)(U val) const 
            if ((s == "*" || s == "/") && isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        Matrix!(T, Cols, Rows) mat;
        foreach (c; CTRange!(0, Cols))
            foreach (r; CTRange!(0, Rows))
                mixin("mat.mData[c][r] = mData[c][r]" ~ s ~ " val;");
        return mat;
    }

    auto opBinaryRight(string s, U)(U val) const 
            if (s == "*" && isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        Matrix!(T, Cols, Rows) mat;
        foreach (c; CTRange!(0, Cols))
            foreach (r; CTRange!(0, Rows))
                mixin("mat.mData[c][r] = val " ~ s ~ " mData[c][r];");
        return mat;
    }

    auto opBinary(string s)(in auto ref Vector!(T, Rows) rhs) const if (s == "*")
    {
        Vector!(T, Rows) vec;
        foreach (c; CTRange!(0, Cols))
            foreach (r; CTRange!(0, Rows))
                vec[r] = vec[r] + (mData[c][r] * rhs[c]);

        return vec;
    }

    auto opBinary(string s, Mat)(in auto ref Mat rhs) const 
            if (s == "*" && is(Mat == Matrix!(T, rCols, Cols), size_t rCols))
    {
        Matrix!(T, rhs.columns, Rows) mat;
        foreach (rC; CTRange!(0, rhs.columns))
            foreach (r; CTRange!(0, Rows))
                foreach (c; CTRange!(0, Cols))
                    mat[rC][r] = mat[rC][r] + (mData[c][r] * rhs.mData[rC][c]);

        return mat;
    }
}

// constructors
unittest
{
    import std.stdio;

    Matrix!(float, 2, 2) m;
    Matrix!(float, 2, 2) m2 = Matrix!(float, 2, 2)(33);
    m2 = Matrix!(float, 2, 2)(1, 2, 3, 4);
    writeln(m2);
}

// accessors

unittest
{
    import std.stdio;

    Matrix!(float, 2, 2) m = Matrix!(float, 2, 2).identity;
    writeln(m);
    Matrix!(float, 4, 4) m2 = Matrix!(float, 4, 4).identity;
    writeln(m2);

    writeln(m[0]);
    writeln(m[1]);

    m[0] = Vector!(float, 2)(10, 30);
    m[1] = Vector!(float, 2)(20, 40);
    writeln(m);

    const(Matrix!(float, 2, 2)) m3 = Matrix!(float, 2, 2).identity;
    writeln(m3[0]);
    writeln(m3[1]);

    writeln(m.ptr);
}

// operators

unittest
{
    import std.stdio;

    Matrix!(float, 2, 2) m = Matrix!(float, 2, 2).identity;
    writeln(m + m);
    writeln(m - m);
    writeln(m * m);
    writeln(m * 10);
    writeln(m / 10);
    writeln(10 * m);

    writeln(m * Vector!(float, 2)(1, 1));
}
