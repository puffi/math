module math.vector;

import std.traits;
import std.meta;

import math.traits;

struct Vector(T, size_t Dims) if (isScalarType!T)
{
    alias type = T;
    alias dimensions = Dims;

    private T[Dims] mData = 0;

    this(Arg)(Arg arg) if (isImplicitlyConvertible!(Arg, T))
    {
        mData[] = cast(T) arg;
    }

    this(Arg)(Arg arg) if (!isImplicitlyConvertible!(Arg, T) && isScalarType!Arg)
    {
        // HACK
        mData[] = cast(T) arg;
    }

    this(T2)(Vector!(T2, Dims) v) if (isImplicitlyConvertible!(T2, T))
    {
        foreach (i; CTRange!(0, Dims))
        {
            mData[i] = cast(T) v.mData[i];
        }
    }

    this(Args...)(Args args)
            if (matchingDimensions!(Dims, Args) && allImplicitlyConvertible!(T, Args))
    {
        foreach (i, arg; args)
        {
            static if (isScalarType!(typeof(arg)))
            {
                mData[mixedArgumentIndex!(i, Args)] = cast(T) arg;
            }
            else static if (is(typeof(arg) == Vector!(T2, d), T2, size_t d))
            {
                foreach (idx; CTRange!(0, arg.dimensions))
                {
                    mData[mixedArgumentIndex!(i + idx, Args)] = cast(T) arg.mData[idx];
                }
            }
            else
                static assert(false, "Invalid type.");
        }
    }

    this(Args...)(Args args)
            if (matchingDimensions!(Dims, Args)
                && (!allImplicitlyConvertible!(T, Args) && allScalarTypes!Args))
    {
        foreach (i, arg; args)
        {
            mData[i] = cast(T) arg;
        }
    }

    // accessors

    @property auto ptr() const
    {
        return mData.ptr;
    }

    static if (Dims >= 1)
    {
        /**
		* Returns the first element.
		*/
        @property T x() const
        {
            return mData[0];
        }

        /**
		* Sets the first element.
		*/
        @property void x(T val)
        {
            mData[0] = val;
        }

        alias r = x;
        alias s = x;
    }
    static if (Dims >= 2)
    {
        /**
		* Returns the second element.
		*/
        @property T y() const
        {
            return mData[1];
        }
        /**
		* Sets the second element.
		*/
        @property void y(T val)
        {
            mData[1] = val;
        }

        alias g = y;
        alias t = y;
    }
    static if (Dims >= 3)
    {
        /**
		* Returns the third element.
		*/
        @property T z() const
        {
            return mData[2];
        }
        /**
		* Sets the third element.
		*/
        @property void z(T val)
        {
            mData[2] = val;
        }

        alias b = z;
        alias p = z;
    }
    static if (Dims == 4)
    {
        /**
		* Returns the fourth element.
		*/
        @property T w() const
        {
            return mData[3];
        }
        /**
		* Sets the fourth element.
		*/
        @property void w(T val)
        {
            mData[3] = val;
        }

        alias a = w;
        alias q = w;
    }

    // swizzling

    @property auto opDispatch(string s)() const if (allValidAccessors!(s, Dims))
    {
        Vector!(T, s.length) ret;
        foreach (i; CTRange!(0, s.length))
        {
            mixin("ret.mData[i] = this." ~ s[i] ~ ";");
        }
        return ret;
    }

    @property void opDispatch(string s, U)(U arg)
            if (allValidAccessors!(s, Dims) && isScalarType!U
                && allImplicitlyConvertible!(T, U) && hasNoDuplicates!s)
    {
        foreach (i; CTRange!(0, s.length))
        {
            mixin(s[i] ~ " = cast(T)arg;");
        }
    }

    @property void opDispatch(string s, U, size_t dims)(in auto ref Vector!(U, dims) arg)
            if (allValidAccessors!(s, Dims) && s.length == dims
                && allImplicitlyConvertible!(T, U) && hasNoDuplicates!s)
    {
        foreach (i; CTRange!(0, s.length))
        {
            mixin(s[i] ~ " = cast(T)arg.mData[i];");
        }
    }

    // operators

    auto opIndex(size_t idx) const
    {
        assert(idx <= Dims, "Index out of range.");
        return mData[idx];
    }

    void opIndexAssign(U)(U arg, size_t idx) if (allImplicitlyConvertible!(T, U))
    {
        assert(idx <= Dims, "Index out of range.");
        mData[idx] = cast(T) arg;
    }

    auto opUnary(string op = "-")() const
    {
        Vector!(T, Dims) v;
        foreach (i; CTRange!(0, Dims))
            v[i] = -mData[i];
        return v;
    }

    auto opBinary(string op, U)(U arg) const 
            if (isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        Vector!(T, Dims) v;
        foreach (i; CTRange!(0, Dims))
            mixin("v[i] = mData[i] " ~ op ~ " arg;");
        return v;
    }

    auto opBinary(string op, U)(U arg) const 
            if (!isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        Vector!(T, Dims) v;
        foreach (i; CTRange!(0, Dims))
            mixin("v[i] = mData[i] " ~ op ~ " arg[i];");
        return v;
    }

    auto opBinaryRight(string op, U)(U arg) const 
            if (isScalarType!U && allImplicitlyConvertible!(T, U))
    {
        Vector!(T, Dims) v;
        foreach (i; CTRange!(0, Dims))
            mixin("v[i] = arg " ~ op ~ " mData[i];");
        return v;
    }
}

unittest
{
    Vector!(int, 2) v;
    Vector!(float, 2) fv;
    v = Vector!(int, 2)(30);
    v = Vector!(int, 2)(30, 40);
    v = Vector!(int, 2)(Vector!(int, 1)(20), 10);
    v = Vector!(int, 2)(10, Vector!(int, 1)(20));
    fv = Vector!(float, 2)(Vector!(int, 2)(20, 30));
}

// accessors
unittest
{
    import std.stdio;

    Vector!(int, 2) v = Vector!(int, 2)(10, 20);
    writeln(v.x);
    writeln(v.y);
    v.x = 30;
    writeln(v.x);
    v.y = 40;
    writeln(v.y);
    Vector!(float, 4) fv = Vector!(float, 4)(10, 20, 30, 40);
    writeln(fv.x);
    writeln(fv.y);
    writeln(fv.z);
    writeln(fv.w);
    fv.x = 40;
    fv.y = 30;
    fv.z = 20;
    fv.w = 10;
    writeln(fv.x);
    writeln(fv.y);
    writeln(fv.z);
    writeln(fv.w);

    writeln(fv.ptr);
}

// swizzling
unittest
{
    import std.stdio;

    Vector!(int, 4) v = Vector!(int, 4)(10, 20, 30, 40);
    writeln(v.xyzw);
    writeln(v.xw);
    writeln(v.zy);
    v.xy = Vector!(int, 2)(20, 10);
    writeln(v.xyzw);
    v.xy = 5;
    writeln(v.xyzw);
    ubyte u = 23;
    v.yw = u;
    writeln(v.xyzw);
}

// operators
unittest
{
    import std.stdio;

    // index
    Vector!(int, 4) v = Vector!(int, 4)(10, 20, 30, 40);
    writeln(v[0]);
    writeln(v[1]);
    writeln(v[2]);
    writeln(v[3]);
    v[0] = 40;
    v[1] = 30;
    v[2] = 20;
    v[3] = 10;
    writeln(v.xyzw);
    // unary
    writeln((-v).xyzw);
    // binary
    writeln(v + v);
    writeln(v - v);
    writeln(v * v);
    writeln(v / v);
    writeln(v + 10);
    writeln(v - 10);
    writeln(v * 10);
    writeln(v / 10);
    writeln(10 + v);
    writeln(10 - v);
    writeln(10 * v);
    writeln(10 / v);
}
